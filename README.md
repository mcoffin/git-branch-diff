# git-branch-diff

The `git branch-diff` command for easily diff'ing branches.

# Usage

```
git branch-diff [BASE_BRANCH] [CURRENT_BRANCH]
```

## Installation

If you are on an Arch-based distribution, `git-branch-diff` is available on the [AUR](https://aur.archlinux.org) as [`git-branch-diff-git`](https://aur.archlinux.org/packages/git-branch-diff-git/).

For other distributions, you can install to `/usr/local` with:

```
make DESTDIR=/usr/local install
```

# Building documentation

```
make doc
```

# License

`git-branch-diff` is licensed under the [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.txt).
