.PHONY: clean doc all install print

.DEFAULT_GOAL = all

DESTDIR ?= /usr/local
DOC_SOURCES := $(shell find . -type f -name '*.scd')
DOC_TARGETS := $(shell find . -type f -name '*.scd' | sed 's/\.scd/\.gz/')
INSTALL_FILES_BIN := git-branch-diff git-current-identifier

%.gz: %.scd
	scdoc < $< | gzip -c > $@

doc: $(DOC_TARGETS)

clean:
	bash -c 'for f in $(DOC_TARGETS); do ([ -e $$f ] && rm $$f) || true; done'

all: doc

install: all
	install -D -m 755 -t "$(DESTDIR)/bin" $(INSTALL_FILES_BIN)
	install -D -m 644 -t "$(DESTDIR)/share/man/man1" $(filter %.1.gz,$(DOC_TARGETS))
